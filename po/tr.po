# translation of tr.po to Turkish
# translation of Gnome-menus.
# Copyright (C) 2004 THE Gnome-menus'S COPYRIGHT HOLDER
# This file is distributed under the same license as the Gnome-menus package.
# Baris Cicek <baris@teamforce.name.tr>, 2005, 2008.
# Onur Can Çakmak <onur.cakmak@gmail.com>, 2006.
msgid ""
msgstr ""
"Project-Id-Version: tr\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-21 15:23+0200\n"
"PO-Revision-Date: 2008-03-10 22:35+0200\n"
"Last-Translator: Baris Cicek <baris@teamforce.name.tr>\n"
"Language-Team: Turkish <gnome-turk@gnome.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: KBabel 1.11.4\n"

#: ../desktop-directories/lxde-audio-video.directory.in.h:1
#, fuzzy
#| msgid "Multimedia menu"
msgid "Multimedia"
msgstr "Çokluortam menüsü"

#: ../desktop-directories/lxde-audio-video.directory.in.h:2
#, fuzzy
#| msgid "Graphics applications"
msgid "Audio and video applications"
msgstr "Grafik uygulamaları"

#: ../desktop-directories/lxde-development.directory.in.h:1
msgid "Development"
msgstr ""

#: ../desktop-directories/lxde-development.directory.in.h:2
msgid "Tools for software development"
msgstr "Yazılım geliştirme için araçlar"

#: ../desktop-directories/lxde-education.directory.in.h:1
msgid "Education"
msgstr "Eğitim"

#: ../desktop-directories/lxde-education.directory.in.h:2
#, fuzzy
#| msgid "Education"
msgid "Educational software"
msgstr "Eğitim"

#: ../desktop-directories/lxde-game.directory.in.h:1
msgid "Games"
msgstr "Oyunlar"

#: ../desktop-directories/lxde-game.directory.in.h:2
msgid "Games and amusements"
msgstr "Oyun ve eğlencelikler"

#: ../desktop-directories/lxde-graphics.directory.in.h:1
msgid "Graphics"
msgstr "Grafik"

#: ../desktop-directories/lxde-graphics.directory.in.h:2
msgid "Graphics applications"
msgstr "Grafik uygulamaları"

#: ../desktop-directories/lxde-hardware.directory.in.h:1
msgid "Hardware"
msgstr "Donanım"

#: ../desktop-directories/lxde-hardware.directory.in.h:2
msgid "Settings for several hardware devices"
msgstr "Bazı donanım aygıtları için ayarlar"

#: ../desktop-directories/lxde-internet-and-network.directory.in.h:1
msgid "Internet and Network"
msgstr "İnternet ve Ağ"

#: ../desktop-directories/lxde-internet-and-network.directory.in.h:2
msgid "Network-related settings"
msgstr "Ağ ile ilgili ayarlar"

#: ../desktop-directories/lxde-look-and-feel.directory.in.h:1
msgid "Look and Feel"
msgstr "Görünüş ve İşleyiş"

#: ../desktop-directories/lxde-look-and-feel.directory.in.h:2
msgid "Settings controlling the desktop appearance and behavior"
msgstr "Masaüstü görünüm ve işleyişini kontrol eden ayarlar"

#: ../desktop-directories/lxde-menu-applications.directory.in.h:1
msgid "Applications"
msgstr "Uygulamalar"

#: ../desktop-directories/lxde-menu-applications.directory.in.h:2
#, fuzzy
#| msgid "Applications"
msgid "Application Menu"
msgstr "Uygulamalar"

#: ../desktop-directories/lxde-menu-system.directory.in.h:1
#: ../desktop-directories/lxde-system.directory.in.h:1
msgid "System"
msgstr "Sistem"

#: ../desktop-directories/lxde-menu-system.directory.in.h:2
msgid "Personal preferences and administration settings"
msgstr "Kişisel tercihler ve yönetim ayarları"

#: ../desktop-directories/lxde-network.directory.in.h:1
msgid "Internet"
msgstr "İnternet"

#: ../desktop-directories/lxde-network.directory.in.h:2
#, fuzzy
#| msgid "Programs for Internet access such as web and email"
msgid "Programs for Internet access"
msgstr "Web ve eposta gibi internete erişim amaçlı programlar"

#: ../desktop-directories/lxde-office.directory.in.h:1
msgid "Office"
msgstr "Ofis"

#: ../desktop-directories/lxde-office.directory.in.h:2
#, fuzzy
#| msgid "Office Applications"
msgid "Office applications"
msgstr "Ofis Uygulamaları"

#: ../desktop-directories/lxde-other.directory.in.h:1
msgid "Other"
msgstr "Diğer"

#: ../desktop-directories/lxde-other.directory.in.h:2
#, fuzzy
#| msgid "Applications that did not fit in other categories"
msgid "Applications that don't fit into other categories"
msgstr "Diğer sınıflandırmalara girmeyen uygulamalar"

#: ../desktop-directories/lxde-personal.directory.in.h:1
msgid "Personal"
msgstr "Kişisel"

#: ../desktop-directories/lxde-personal.directory.in.h:2
msgid "Personal settings"
msgstr "Kişisel ayarlar"

#: ../desktop-directories/lxde-science-math.directory.in.h:1
msgid "Science & Math"
msgstr "Bilim ve Matematik"

#: ../desktop-directories/lxde-science-math.directory.in.h:2
msgid "Scientific and mathematical software"
msgstr ""

#: ../desktop-directories/lxde-science.directory.in.h:1
#, fuzzy
#| msgid "Science & Math"
msgid "Science"
msgstr "Bilim ve Matematik"

#: ../desktop-directories/lxde-science.directory.in.h:2
msgid "Scientific software"
msgstr ""

#: ../desktop-directories/lxde-settings.directory.in.h:1
msgid "Preferences"
msgstr "Tercihler"

#: ../desktop-directories/lxde-settings.directory.in.h:2
#, fuzzy
#| msgid "Personal preferences"
msgid "Desktop preferences"
msgstr "Kişisel tercihler"

#: ../desktop-directories/lxde-settings-system.directory.in.h:1
msgid "Administration"
msgstr "Yönetim"

#: ../desktop-directories/lxde-settings-system.directory.in.h:2
#, fuzzy
#| msgid "Change system-wide settings (affects all users)"
msgid "System-wide settings (affecting all users)"
msgstr "Sistem geneli ayarları değiştir (tüm kullanıcıları etkiler)"

#: ../desktop-directories/lxde-system.directory.in.h:2
#, fuzzy
#| msgid "System configuration and monitoring"
msgid "System tools and monitoring"
msgstr "Sistem yapılandırma ve izleme"

#: ../desktop-directories/lxde-utility-accessibility.directory.in.h:1
msgid "Universal Access"
msgstr "Evrensel Erişim"

#: ../desktop-directories/lxde-utility-accessibility.directory.in.h:2
msgid "Universal Access Settings"
msgstr "Evrensel Erişim Ayarları"

#: ../desktop-directories/lxde-utility.directory.in.h:1
msgid "Accessories"
msgstr "Donatılar"

#: ../desktop-directories/lxde-utility.directory.in.h:2
msgid "Desktop accessories"
msgstr "Masaüstü donatıları"

#~ msgid "Sound & Video"
#~ msgstr "Ses ve Video"

#~ msgid "Programming"
#~ msgstr "Programlama"

#~ msgid "System settings"
#~ msgstr "Sistem ayarları"

#~ msgid "System Tools"
#~ msgstr "Sistem Araçları"

#~ msgid "Universal access related preferences"
#~ msgstr "Evrensel erişim ile ilgili tercihler"

#~ msgid "Personal preferences and settings"
#~ msgstr "Kişisel tercih ve ayarlar"

#~ msgid "Menu Editor"
#~ msgstr "Menü Düzenleyicisi"

#~ msgid "Edit Menus"
#~ msgstr "Menüleri Düzenle"

#~ msgid "_Applications:"
#~ msgstr "_Uygulamalar:"

#~ msgid "_Defaults"
#~ msgstr "Ö_ntanımlılar"

#~ msgid "_Menus:"
#~ msgstr "_Menüler:"

#~ msgid "Name"
#~ msgstr "Ad"

#~ msgid "Show"
#~ msgstr "Göster"

#~ msgid ""
#~ "Cannot find home directory: not set in /etc/passwd and no value for $HOME "
#~ "in environment"
#~ msgstr ""
#~ "Başlangıç dizini bulunamadı: /etc/passwd içinde ayarlanmamış ve ortam "
#~ "değişkeni $HOME içinde değer yok"

#~ msgid "Menu file"
#~ msgstr "Menü dosyası"

#~ msgid "MENU_FILE"
#~ msgstr "MENU_DOSYASI"

#~ msgid "Monitor for menu changes"
#~ msgstr "Menü değişikliklerini izle"

#~ msgid "Include <Exclude>d entries"
#~ msgstr "<Exclude> edilmiş girişleri içer"

#~ msgid "Include NoDisplay=true entries"
#~ msgstr "NoDisplay=true girdileri içer"

#~ msgid "Invalid desktop file ID"
#~ msgstr "Geçersiz masaüstü dosya ID'si"

#~ msgid "[Invalid Filename]"
#~ msgstr "[Geçersiz Dosyaadı]"

#~ msgid " <excluded>"
#~ msgstr " <excluded>"

#~ msgid ""
#~ "\n"
#~ "\n"
#~ "\n"
#~ "==== Menu changed, reloading ====\n"
#~ "\n"
#~ "\n"
#~ msgstr ""
#~ "\n"
#~ "\n"
#~ "\n"
#~ "==== Menu değişti, yeniden yükleniyor ====\n"
#~ "\n"
#~ "\n"

#~ msgid "Menu tree is empty"
#~ msgstr "Menü ağacı boş"

#~ msgid "- test GNOME's implementation of the Desktop Menu Specification"
#~ msgstr "- GNOME'un Masaüstü Menü Belirtimleri'ni sına"
